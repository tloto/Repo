Pod::Spec.new do |s|
  s.name             = 'KzhUtils'
  s.version          = '1.0.0'
  s.summary          = 'A short description of KzhUtils.'

  s.homepage         = 'https://gitlab.com/tloto/KzhUtils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '1162917226@qq.com' => '1162917226@qq.com' }
  s.source           = { :git => 'https://gitlab.com/tloto/KzhUtils', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'KzhUtils/**/*'
 
  
  # s.resource_bundles = {
  #   'KzhUtils' => ['KzhUtils/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'


end

